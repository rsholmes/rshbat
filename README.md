# RSHbat

Themes for [bat](https://github.com/sharkdp/bat), slight variant of [Coldark](https://github.com/ArmandPhilippot/coldark-bat) themes.

## License

This project is open source and available under the [MIT License](https://github.com/ArmandPhilippot/coldark-bat/blob/master/LICENSE).

